<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;


class ItemsController extends Controller
{

	public function getAllItems()
	{
		echo "string";
	}

	public function createCheklistItem(Request $request)
	{
		/*

		INPUT RAW

		{
		  "data": {
		    "attribute": {
		      "description": "Need to verify this guy house.",
		      "due": "2019-01-19 18:34:51",
		      "urgency": "2",
		      "assignee_id": 123
		    }
		  }
		}
		*/
		$input = $request->json()->all();

		$store = Item::create($input['data']['attribute']);
		$newId = $store->id;

		if($store){

			$result = Item::find($newId);


			/*
			
			RESPONSE HARUSNYA

			{
			  "data": {
			    "type": "checklists",
			    "id": 1,
			    "attributes": {
			      "description": "Example Item.",
			      "is_completed": false,
			      "completed_at": null,
			      "due": null,
			      "urgency": 0,
			      "updated_by": null,
			      "updated_at": null,
			      "created_at": "2018-01-25T07:50:14+00:00"
			    },
			    "links": {
			      "self": "https://dev-kong.command-api.kw.com/checklists/50127"
			    }
			  }
			}

			*/

			$response = [ "message" => "success","code" => 200, "result"  => $result->toArray()];

		} else {
			$response = [ "message" => "failed","code" => 401, "result"  => false];
		}

		return response()->json($response, $response['code']);


	}

}
