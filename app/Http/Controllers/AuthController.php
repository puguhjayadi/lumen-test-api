<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Http\Request;


class AuthController extends Controller
{

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);

        $user = User::where("email", $request->input("email"))->first();

        if (!$user) {
            $response = [
                "message" => "login_vailed",
                "code"    => 401,
                "result"  => [
                    "token" => null,
                ]
            ];
            return response()->json($response, $response['code']);
        }

        if (Hash::check($request->input("password"), $user->password)) {
            $generateToken  = Str::random(10);

            $user->update([
                'token' => $generateToken
            ]);

            $response = [
                "message" => "login_success",
                "code"    => 200,
                "result"  => [
                    "token" => $generateToken,
                ]
            ];
        } else {
            $response = [
                "message" => "login_failed",
                "code"    => 401,
                "result"  => [
                    "token" => null,
                ]
            ];
        }

        return response()->json($response, $response['code']);
    }

}
