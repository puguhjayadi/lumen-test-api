<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string("description");
            $table->boolean("is_completed")->default(0);
            $table->string("completed_at")->nullable();
            $table->dateTime('due');
            $table->integer('due_interval')->nullable();
            $table->string('due_unit')->nullable();
            $table->integer("urgency");
            $table->string("updated_by")->nullable();
            $table->string("created_by")->nullable();
            $table->integer("assignee_id");
            $table->integer("task_id")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}