<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableChecklists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('checklists', function (Blueprint $table) {
        $table->increments('id')->unsigned();
        $table->string("object_domain");
        $table->string("object_id");
        $table->string("description");
        $table->boolean("is_completed")->default(0);
        $table->string("completed_at")->nullable();
        $table->dateTime('due')->nullable();
        $table->integer("urgency")->default(0);
        $table->string("updated_by");
        $table->string("created_by");
        $table->integer("assignee_id");
        $table->integer("task_id");
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('checklists');
    }
  }


/*

{
  "type": "object",
  "title": "Cheklist Attributes",
  "properties": {
    "object_domain": {
      "type": "string"
    },
    "object_id": {
      "type": "string"
    },
    "description": {
      "type": "string"
    },
    "is_completed": {
      "type": "boolean"
    },
    "completed_at": {
      "type": "null"
    },
    "updated_by": {
      "type": "string"
    },
    "updated_at": {
      "type": "null"
    },
    "created_at": {
      "type": "string"
    },
    "due": {
      "type": [
        "string",
        "null"
      ],
      "format": "date-time",
      "example": "2020-01-29 11:44:40"
    },
    "urgency": {
      "type": "integer"
    }
  },
  "required": [
    "object_domain",
    "object_id",
    "description"
  ]
}*/